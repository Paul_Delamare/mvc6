<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\MailPerso;
use App\Service\SecurityService;
use App\Service\Validation;
use Core\Kernel\AbstractController;
use Core\Kernel\Mail;
use http\Client\Curl\User;

class SecurityController extends BaseController{

    public function login(){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
                $user=UserModel::loginUser($post);
                if (!empty($user)){
                    if (password_verify($post['password'], $user->password)){
                        $_SESSION['verifLogin']=array(
                            'id'=>$user->id,
                            'email'=>$user->email,
                            'ip'=>$_SERVER['REMOTE_ADDR'],
                        );
                        UserModel::addLogin($user->id);

                        $this->redirect('admin');
                    }else{
                        $error['password']='Crédential';
                    }
                }else{
                    $error['password']='Crédential';
                }
            }
        }


        $form=new Form($error);
        $isPageRegister=false;
        $this->render('app.security.login', array(
            'form'=> $form,
            'isPageRegister'=>$isPageRegister,
        ));
    }

    public function register(){
        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v=new Validation();
            $error=$this->validateExistUser($v, $post);
            $this->dump($error);

            if ($v->IsValid($error)){
                $password=password_hash($post['password'], PASSWORD_DEFAULT);
                $token=new SecurityService();
                $newToken=$token->generateRandomString();
                $data=[
                    'email'=>$post['email'],
                    'password'=>$password,
                    'token'=>$newToken,
                ];
                UserModel::insertUser($data);
                $sendMail= new MailPerso();
                $sendMail->sendMailRegister($post['email']);
//                MailPerso::sendMailRegister($post['email']);
//                $this->dump($data);
                $this->redirect('home');
            }
        }
        $form=new Form($error);
        $isPageRegister=true;
        $this->render('app.security.register', array(
            'form'=>$form,
            'isPageRegister'=>$isPageRegister,
        ));
    }
    public function logout(){
        $_SESSION=array();
        session_destroy();
        $this->redirect('admin');
    }

    public function update(){
        $error=[];
        $lien='';
        if (!empty($_POST['submitted'])){
            $post= $this->cleanXss($_POST);
            $v= new Validation();
            $error['email']=$v->validationEmail($post['email']);

            if ($v->IsValid($error)){
                $link=UserModel::fetchToken($post['email']);
//                $this->dump($link);
                if (!empty($link)){
                    $this->dump($link);
                    $lien= '<a href="newpassword?email='.urlencode($link->email).'&token='.urlencode($link->token).'">Cliquer ici pour modifier votre mot de passe</a>';
                }
            }
        }
        $form=new Form($error);
        $this->render('app.security.modifpassword', array(
            'form'=>$form,
            'lien'=>$lien,
        ));
    }

    public function newpassword(){
        $error=[];
        $email= urldecode($_GET['email']);
        $token=urldecode($_GET['token']);
        $isPageRegister=true;
        $form= new Form($error);
        if (!empty($_GET)){
            $verifEmail=UserModel::fetchToken($email);
            if ($verifEmail->token===$token){
                if (!empty($_POST['submitted'])){
                    $post=$this->cleanXss($_POST);
                    $v=new Validation();
                    $error['password']=$v->verifAllPassword($post['password'], $post['password2'], 4);
                    if ($v->IsValid($error)){
                        $password=password_hash($post['password'], PASSWORD_DEFAULT);
                        $token=new SecurityService();
                        $newToken=$token->generateRandomString();
                        $data=[
                            'email'=>$post['email'],
                            'password'=>$password,
                            'token'=>$newToken,
                        ];
                        UserModel::newToken($data);
                        $this->redirect('admin');
                    }
                }
            }
        }
        $this->render('app.security.newpassword', array(
            'isPageRegister'=>$isPageRegister,
            'form'=>$form,
        ));
    }

    private function validateExistUser($v, $post){
        $error=[];
        $error['email']=$v->validationEmail($post['email']);
        if ($error['email']===''){
            $error['email']=$v->existUser($post['email']);
        }
        $error['password']=$v->verifAllPassword($post['password'], $post['password2'], 4);
        return $error;
    }
    private function validate($v, $post){
        $error=[];
        $error['email']=$v->validationEmail($post['email']);
        $error['password']=$v->textValid($post['password'], 'password',4 , 255);
        return $error;
    }
}