<?php
namespace App\Controller;

use App\Model\ContactModel;
use App\Service\Form;
use App\Service\MailPerso;
use App\Service\Validation;

class ContactController extends BaseController{

    public function contact(){

        $error=[];
        if (!empty($_POST['submitted'])){
            $post=$this->cleanXss($_POST);
            $v= new Validation();
            $error=$this->validate($v, $post);
            if ($v->IsValid($error)){
                ContactModel::sendContact($post);
                $sendmail= new MailPerso();
                if(!empty($_SESSION)){
                    $sendmail->sendContact($_SESSION['verifLogin']['email'], $post['content'], $post['objet']);
                }else{
                    $sendmail->sendContact($post['email'], $post['content'], $post['objet']);
                }
                $this->redirect('home');
            }
        }
        $form= new Form($error);
        $this->render('app.contact.contact', array(
            'form'=>$form,

        ), 'admin');
    }

    private function validate($v, $post){
        $error=[];
        if (empty($_SESSION)){
            $error['email']=$v->emailValid($post['email']);
        }
        $error['objet']=$v->textValid($post['objet'], 'objet', 4, 100);
        $error['content']=$v->textValid($post['content'], 'contenu',4 , 1000);
        return $error;
    }
    public function exemple(){
        $this->render('app.contact.exemple');
    }
}