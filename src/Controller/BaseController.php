<?php

namespace App\Controller;

use App\Service\SecurityService;
use Core\Kernel\AbstractController;

class BaseController extends AbstractController{

    protected $user;

    public function islogged(){
        $de = new SecurityService();
        return $de->isLogged();
    }


}