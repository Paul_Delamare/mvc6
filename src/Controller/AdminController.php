<?php

namespace App\Controller;

use Core\Kernel\AbstractController;
//require '../vendor/autoload.php';

use JasonGrimes\Paginator;

class AdminController extends BaseController{

    public function index(){
        $currentPage = 1;
        $offset=0;
        if (!empty($_GET['page'])){
            $currentPage = $_GET['page'];
        }
        $isPage=true;
        $totalItems = 100;
        $itemsPerPage = 10;
        $urlPattern = '/admin?page=(:num)';
        $paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
        $this->dump($_SESSION);

        $this->render('app.admin.index', array(
            'isPage'=>$isPage,
            'paginator'=>$paginator,
        ), 'admin');
    }
    public function model(){
        $this->render('app.admin.mailmodel');
    }
}