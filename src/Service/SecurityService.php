<?php

namespace App\Service;

class SecurityService{

    public function __construct(){

    }
    public function generateRandomString($length = 40) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function isLogged() {
        if(!empty($_SESSION['verifLogin']['id'])) {
            if(!empty($_SESSION['verifLogin']['email'])) {
                if(!empty($_SESSION['verifLogin']['ip'])) {
                    if($_SESSION['verifLogin']['ip'] == $_SERVER['REMOTE_ADDR']) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}