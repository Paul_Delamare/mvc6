<?php

namespace App\Service;
use Core\Kernel\Mail;

class MailPerso extends Mail{
    public function sendMessage($email, $message){
        $content=$this->contentAssembly('app.email.app.content',array(
            'message'=>$message,
        ));
        $mail=new Mail();
        $mail->sendMail($content, $email);
    }
    public function sendMailRegister($email){
        $content=$this->contentAssembly('app.email.app.email-register');
        $mail= new Mail();
        $mail->sendMail($content, $email);
    }
    public  function sendContact($email, $content, $subject){
        $autre='super message';
        $encore='voillaaaaa';
        $envoi=$this->contentAssembly('app.email.app.message-envoye');
        $content=$this->contentAssembly('app.email.app.send-contact',array(
            'content'=>$content,
            'autre'=>$autre,
            'encore'=>$encore,
        ));
        $mail= new Mail();
        $mail->sendMail($envoi, $email, 'Message envoyé !');
        $mail->sendMail($content, $email, $subject, true);
    }
}


