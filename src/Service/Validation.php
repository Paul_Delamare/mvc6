<?php
namespace App\Service;

use App\Model\UserModel;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {
        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }
    public function validationEmail($mail1){
        $error='';
        if (!empty($mail1)){
            if (!filter_var($mail1, FILTER_VALIDATE_EMAIL)){
                $error='Veuillez renseignez un email valide*';
            }
        }else{
            $error= 'Veuillez rentrer une adresse email*';
        }
        return $error;
    }
    public function existUser($email){
        $error=[];
        $existEmail=UserModel::findUser($email);
        if (!empty($existEmail)){
            $error='Cette adresse mail existe déjà';
        }
        return $error;
    }
    public function verifAllPassword( $password, $password2, $min){
        $error='';
        if(!empty($password) && !empty($password2)) {
            if($password != $password2) {
                $error = 'Vos mots de passe sont différents*';
            } elseif(mb_strlen($password) < $min) {
                $error = 'Votre mot de passe est trop court(min '.$min.')';
            }
        } else {
            $error = 'Veuillez renseigner les mots de passe*';
        }
        return $error;
    }

}
