<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ContactModel extends AbstractModel{

    protected static $table = 'contact';

    public static function sendContact($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (objet, content, send_at, email) VALUES ( ? ,?, NOW(), ?)", array($post['objet'], $post['content'], $post['email']));
    }
}