<?php

namespace App\Model;

use Core\Kernel\AbstractModel;
use Core\App;

class UserModel extends AbstractModel{
    protected static $table = 'user2';

    public static function findUser($email){
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE email = ?",[$email],get_called_class(),true);
    }
    public static function insertUser($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (email, password, token, created_at) VALUES ( ? ,?, ?, NOW())", array($post['email'], $post['password'], $post['token']));
    }
    public static function loginUser($post){
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE email = ? ",[$post['email']],get_called_class(),true);
    }
    public static function addLogin($id){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET count_login = count_login + 1 WHERE id = ? ", array($id));
    }
    public static function fetchToken($email){
        return App::getDatabase()->prepare("SELECT email, token FROM " . self::getTable() . " WHERE email = ? ",[$email],get_called_class(),true);
    }
    public static function newToken($data){
        App::getDatabase()->prepareInsert("UPDATE ".self::$table." SET password = ? token = ?  WHERE email = ? ", array($data['password'], $data['token'],$data['email']));
    }
}