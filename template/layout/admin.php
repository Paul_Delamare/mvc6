<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash()) ?>
<header id="masthead">
    <nav>
        <ul>
            <li><a href="<?= $view->path(''); ?>">Home</a></li>
            <li><a href="<?= $view->path('admin'); ?>">Admin</a></li>
            <li><a href="<?= $view->path('model'); ?>">Model</a></li>
            <li><a href="<?= $view->path('contact'); ?>">Contact</a></li>
            <li><a href="<?= $view->path('exemple'); ?>">Exemple</a></li>
            <?php
            if (!$view->islogged()){
                ?>
                <li><a href="<?= $view->path('login'); ?>">Login</a></li>
                <li><a href="<?= $view->path('register'); ?>">Register</a></li>
                <?php
            }else{
                ?>
                <li><a href="<?= $view->path('logout'); ?>">Logout</a></li>
                <?php
            }
            ?>
        </ul>
    </nav>
</header>

<div class="container">
    <?= $content; ?>
</div>

<footer id="colophon">
    <div class="wrap">
        <p>MVC 6 - Framework Pédagogique.</p>
    </div>
</footer>
<?php echo $view->add_webpack_script('admin'); ?>
<?php if (!empty($isPage)){
    if ($isPage){
        echo $view->add_webpack_script('test');
    }
}   ?>
</body>
</html>