<form method="post" action="">
    <div class="email">
        <?php if (!$view->islogged()){
            echo $form->label('email');
            echo $form->input('email', 'email');
            echo $form->error('email');
        }?>
    </div>
    <div class="objet_contact">
        <?php echo $form->label('objet'); ?>
        <?php echo $form->input('objet', ''); ?>
        <?php echo $form->error('objet'); ?>
    </div>
    <div class="content_contact">
        <?php echo $form->label('content'); ?>
        <?php echo $form->textarea('content'); ?>
        <?php echo $form->error('content'); ?>
    </div>
    <?php echo $form->submit(); ?>
</form>