<section id="forgetpassword">
    <div class="wrap">
        <form action="" method="post" novalidate>
            <?php echo $form->label('email'); ?>
            <?php echo $form->input('email', 'email'); ?>
            <?php echo $form->error('email'); ?>

            <?php echo $form->submit(); ?>
        </form>
    </div>
</section>
<?php
echo $lien;