<section id="newpassword">
    <div class="wrap">
        <?php echo $form->label('password'); ?>
        <?php echo $form->input('password', 'password'); ?>
        <?php echo $form->error('password'); ?>

        <?php echo $form->label('password2'); ?>
        <?php echo $form->input('password2', 'password'); ?>
        <?php echo $form->error('password2'); ?>

        <?php echo $form->submit(); ?>
    </div>
</section>