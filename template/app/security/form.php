<form action="" method="post" novalidate>
    <?php echo $form->label('email'); ?>
    <?php echo $form->input('email', 'email', $poney->email ?? ''); ?>
    <?php echo $form->error('email'); ?>

    <?php echo $form->label('password'); ?>
    <?php echo $form->input('password', 'password', $poney->password ?? '' ); ?>
    <?php echo $form->error('password'); ?>
    <?php if ($isPageRegister){
        ?>
        <?php echo $form->label('password2'); ?>
        <?php echo $form->input('password2', 'password', $poney->password2 ?? '' ); ?>
        <?php
    } ?>

    <?php echo $form->submit(); ?>
</form>