<?php

$routes = array(
    array('home','default','index'),

    array('admin','admin','index'),
    array('model','admin','model'),

    array('login','security','login'),
    array('register','security','register'),
    array('logout','security','logout'),
    array('modifpassword','security','update'),
    array('newpassword','security','newpassword'),

    array('contact', 'contact', 'contact'),
    array('exemple', 'contact', 'exemple'),
);









